import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:quizzler/question.dart';

class QuizBrain {
  int questionNumber = 0;
  List<Question> _questionBank = [
    Question(questionText: 'Pregunta 1', questionAnswer: true),
    Question(questionText: 'Pregunta 2', questionAnswer: false),
    Question(questionText: 'Pregunta 3', questionAnswer: false),
    Question(questionText: 'Pregunta 4', questionAnswer: true),
    Question(questionText: 'Pregunta 5', questionAnswer: false),
  ];


  String getQuestionText() {
    return _questionBank[questionNumber].questionText;
  }

  bool getCorrectAnswer() {
    return _questionBank[questionNumber].questionAnswer;
  }

  nextQuestion(BuildContext context) {
    (questionNumber == _questionBank.length - 1)
        ? questionNumber=0
        : questionNumber++;
  }
}
